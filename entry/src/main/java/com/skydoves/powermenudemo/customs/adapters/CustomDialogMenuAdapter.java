/*
 * Copyright (C) 2017 skydoves
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.skydoves.powermenudemo.customs.adapters;

import com.skydoves.powermenu.MenuBaseAdapter;
import com.skydoves.powermenudemo.ResourceTable;
import com.skydoves.powermenudemo.customs.items.NameCardMenuItem;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;

@SuppressWarnings("ConstantConditions")
public class CustomDialogMenuAdapter extends MenuBaseAdapter<NameCardMenuItem> {
    public CustomDialogMenuAdapter() {
        super();
    }

    @Override
    public Component getComponent(int index, Component view, ComponentContainer viewGroup) {
        final Context context = viewGroup.getContext();
        if (view == null) {
            LayoutScatter inflater = LayoutScatter.getInstance(context);
            view = inflater.parse(ResourceTable.Layout_item_name_card, viewGroup, false);
        }

        NameCardMenuItem item = (NameCardMenuItem) getItem(index);
        final Image icon = (Image) view.findComponentById(ResourceTable.Id_item_name_card_profile);
        icon.setImageElement(item.getIcon());
        final Text name = (Text) view.findComponentById(ResourceTable.Id_item_name_card_name);
        name.setText(item.getName());
        final Text content = (Text) view.findComponentById(ResourceTable.Id_item_name_card_board);
        content.setText(item.getContent());
        return super.getComponent(index, view, viewGroup);
    }
}
