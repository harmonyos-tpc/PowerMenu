package com.skydoves.powermenudemo;

import com.skydoves.powermenu.CustomPowerMenu;
import com.skydoves.powermenu.OnDismissedListener;
import com.skydoves.powermenu.OnMenuItemClickListener;
import com.skydoves.powermenu.PowerMenu;
import com.skydoves.powermenu.PowerMenuItem;

import com.skydoves.powermenu.TransPowerMenu;
import com.skydoves.powermenudemo.customs.adapters.CenterMenuAdapter;
import com.skydoves.powermenudemo.customs.adapters.CustomDialogMenuAdapter;
import com.skydoves.powermenudemo.customs.items.NameCardMenuItem;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;

public class MainAbility extends Ability implements Component.ClickedListener {
    private Image hamburgerImg;
    private Image profileImg;
    private Image floatImg;

    private Image proImg1;
    private Image proImg2;
    private Image proImg3;

    private Image shareImg1;
    private Image shareImg2;
    private Image shareImg3;

    private PowerMenu hamburgerMenu;
    private TransPowerMenu profileMenu;
    private CustomPowerMenu<String, CenterMenuAdapter> writeMenu;
    private CustomPowerMenu<String, CenterMenuAdapter> alertMenu;
    private PowerMenu dialogMenu;
    private CustomPowerMenu<NameCardMenuItem, CustomDialogMenuAdapter> customDialogMenu;
    private PowerMenu iconMenu;
    private final OnMenuItemClickListener<PowerMenuItem> onHamburgerItemClickListener =
            new OnMenuItemClickListener<PowerMenuItem>() {
                @Override
                public void onItemClick(int position, PowerMenuItem item) {
                    hamburgerMenu.setSelectedPosition(position);
                    hamburgerMenu.dismiss();
                }
            };
    private final OnDismissedListener onHamburgerMenuDismissedListener =
            () ->
                    new ToastDialog(getContext()).setText("onDismissed hamburger menu").show();
    private final OnMenuItemClickListener<PowerMenuItem> onProfileItemClickListener =
            new OnMenuItemClickListener<PowerMenuItem>() {
                @Override
                public void onItemClick(int position, PowerMenuItem item) {
                    new ToastDialog(getContext()).setText(item.getTitle().toString()).show();
                    profileMenu.dismiss();
                }
            };
    private final OnMenuItemClickListener<String> onWriteItemClickListener =
            new OnMenuItemClickListener<String>() {
                @Override
                public void onItemClick(int position, String title) {
                    new ToastDialog(getContext()).setText(title).show();
                    writeMenu.dismiss();
                }
            };
    private final OnMenuItemClickListener<String> onAlertItemClickListener =
            new OnMenuItemClickListener<String>() {
                @Override
                public void onItemClick(int position, String title) {
                    new ToastDialog(getContext()).setText(title).show();
                    alertMenu.dismiss();
                }
            };
    private final OnMenuItemClickListener<PowerMenuItem> onIconMenuItemClickListener =
            new OnMenuItemClickListener<PowerMenuItem>() {
                @Override
                public void onItemClick(int position, PowerMenuItem item) {
                    new ToastDialog(getContext()).setText(item.getTitle().toString()).show();
                    iconMenu.dismiss();
                }
            };

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_main);
        hamburgerMenu =
                PowerMenuUtils.getHamburgerPowerMenu(
                        this, this, onHamburgerItemClickListener, onHamburgerMenuDismissedListener);
        profileMenu = PowerMenuUtils.getProfilePowerMenu(this, this, onProfileItemClickListener);
        writeMenu = PowerMenuUtils.getWritePowerMenu(this, this, onWriteItemClickListener);
        alertMenu = PowerMenuUtils.getAlertPowerMenu(this, this, onAlertItemClickListener);
        iconMenu = PowerMenuUtils.getIconPowerMenu(this, this, onIconMenuItemClickListener);

        hamburgerImg = (Image) findComponentById(ResourceTable.Id_onHamburger);
        profileImg = (Image) findComponentById(ResourceTable.Id_onProfile);
        floatImg = (Image) findComponentById(ResourceTable.Id_onWrite);

        proImg1 = (Image) findComponentById(ResourceTable.Id_profile1);
        proImg2 = (Image) findComponentById(ResourceTable.Id_profile2);
        proImg3 = (Image) findComponentById(ResourceTable.Id_profile3);

        shareImg1 = (Image) findComponentById(ResourceTable.Id_onShare1);
        shareImg2 = (Image) findComponentById(ResourceTable.Id_onShare2);
        shareImg3 = (Image) findComponentById(ResourceTable.Id_onShare3);

        hamburgerImg.setClickedListener(this);
        profileImg.setClickedListener(this);
        floatImg.setClickedListener(this);

        proImg1.setClickedListener(this);
        proImg2.setClickedListener(this);
        proImg3.setClickedListener(this);

        shareImg1.setClickedListener(this);
        shareImg2.setClickedListener(this);
        shareImg3.setClickedListener(this);

        initializeDialogMenu();
        initializeCustomDialogMenu();
    }

    private void initializeDialogMenu() {
        dialogMenu = PowerMenuUtils.getDialogPowerMenu(this, this);
        Component footerView = dialogMenu.getFooterView();
        Text textViewYes = (Text) footerView.findComponentById(ResourceTable.Id_textView_yes);
        textViewYes.setClickedListener(
                view -> {
                    new ToastDialog(getContext()).setText("Yes").show();
                    dialogMenu.dismiss();
                });
        Text textViewNo = (Text) footerView.findComponentById(ResourceTable.Id_textView_no);
        textViewNo.setClickedListener(
                view -> {
                    new ToastDialog(getContext()).setText("No").show();
                    dialogMenu.dismiss();
                });
    }

    private void initializeCustomDialogMenu() {
        customDialogMenu = PowerMenuUtils.getCustomDialogPowerMenu(this, this);
        Component footerView = customDialogMenu.getFooterView();
        Text textViewYes = (Text) footerView.findComponentById(ResourceTable.Id_textView_yes);
        textViewYes.setClickedListener(
                view -> {
                    new ToastDialog(getContext()).setText("Read more").show();
                    customDialogMenu.dismiss();
                });
        Text textViewNo = (Text) footerView.findComponentById(ResourceTable.Id_textView_no);
        textViewNo.setClickedListener(
                view -> {
                    new ToastDialog(getContext()).setText("close").show();
                    customDialogMenu.dismiss();
                });
    }

    public void onHamburger(Component view) {
        if (hamburgerMenu.isShowing()) {
            hamburgerMenu.dismiss();
            return;
        }
        hamburgerMenu =
                PowerMenuUtils.getHamburgerPowerMenu(
                        this, this, onHamburgerItemClickListener, onHamburgerMenuDismissedListener);
        hamburgerMenu.showLocation(view);
    }

    public void onProfile(Component anchor) {
        int[] locations = anchor.getLocationOnScreen();
        int yOff = locations[1] + anchor.getEstimatedHeight();
        profileMenu = PowerMenuUtils.getProfilePowerMenu(this, this, onProfileItemClickListener);
        profileMenu.showTransAsAnchor(anchor, 270, yOff);
    }

    public void onDialog(Component view) {
        if (dialogMenu.isShowing()) {
            dialogMenu.dismiss();
            return;
        }
        initializeDialogMenu();
        dialogMenu.showAtCenter(view);
    }

    public void onCustomDialog(Component view) {
        if (customDialogMenu.isShowing()) {
            customDialogMenu.dismiss();
            return;
        }
        initializeCustomDialogMenu();
        customDialogMenu.showAtCenter(view);
    }

    public void onWrite(Component view) {
        if (writeMenu.isShowing()) {
            writeMenu.dismiss();
            return;
        }
        writeMenu = PowerMenuUtils.getWritePowerMenu(this, this, onWriteItemClickListener);
        writeMenu.showAtCenter(view);
    }

    public void onAlert(Component view) {
        if (alertMenu.isShowing()) {
            alertMenu.dismiss();
            return;
        }
        alertMenu = PowerMenuUtils.getAlertPowerMenu(this, this, onAlertItemClickListener);
        alertMenu.showAtCenter(view);
    }

    public void onShare(Component view) {
        if (iconMenu.isShowing()) {
            iconMenu.dismiss();
            return;
        }
        iconMenu = PowerMenuUtils.getIconPowerMenu(this, this, onIconMenuItemClickListener);
        iconMenu.showAsDropDown(view, -400, -50);
    }

    @Override
    public void onBackPressed() {
        if (hamburgerMenu.isShowing()) {
            hamburgerMenu.dismiss();
        } else if (profileMenu.isShowing()) {
            profileMenu.dismiss();
        } else if (writeMenu.isShowing()) {
            writeMenu.dismiss();
        } else if (alertMenu.isShowing()) {
            alertMenu.dismiss();
        } else if (dialogMenu.isShowing()) {
            dialogMenu.dismiss();
        } else if (customDialogMenu.isShowing()) {
            customDialogMenu.dismiss();
        } else if (iconMenu.isShowing()) {
            iconMenu.dismiss();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_onHamburger:
                onHamburger(component);
                break;
            case ResourceTable.Id_onProfile:
                onProfile(component);
                break;
            case ResourceTable.Id_onWrite:
                onWrite(component);
                break;
            case ResourceTable.Id_profile1:
                onAlert(component);
                break;
            case ResourceTable.Id_profile2:
                onDialog(component);
                break;
            case ResourceTable.Id_profile3:
                onCustomDialog(component);
                break;
            case ResourceTable.Id_onShare1:
            case ResourceTable.Id_onShare2:
            case ResourceTable.Id_onShare3:
                onShare(component);
                break;
            default:
                break;
        }
    }
}
