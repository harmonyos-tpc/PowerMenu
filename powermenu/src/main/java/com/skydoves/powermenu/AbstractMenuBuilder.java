/*
 * Copyright (C) 2017 skydoves
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.skydoves.powermenu;

import com.skydoves.powermenu.annotations.Dp;

import ohos.aafwk.ability.ILifecycle;
import ohos.aafwk.ability.Lifecycle;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * AbstractMenuBuilder is the abstract builder class of {@link PowerMenu.Builder} and {@link
 * CustomPowerMenu.Builder}.
 */
public abstract class AbstractMenuBuilder {
    protected Context context;
    protected LayoutScatter layoutInflater;
    protected boolean showBackground = true;
    protected ILifecycle iLifeCycle = null;
    protected Component.ClickedListener backgroundClickListener = null;
    protected OnDismissedListener onDismissedListener = null;
    protected MenuAnimation menuAnimation = MenuAnimation.DROP_DOWN;
    protected Component headerView = null;
    protected Component footerView = null;
    protected int animationStyle = -1;
    protected float menuRadius = 5;
    protected float menuShadow = 5;
    protected int width = 0;
    protected int height = 0;
    protected int padding = 0;
    protected int dividerHeight = 0;
    protected int backgroundColor = Color.BLACK.getValue();
    @Dp
    protected int iconSize = 35;
    @Dp
    protected int iconPadding = 7;
    protected int iconColor = -2;
    protected boolean focusable = false;
    protected int selected = -1;
    protected boolean isClipping = true;
    protected boolean autoDismiss = false;
    protected boolean dismissIfShowAgain = true;
    protected String preferenceName = null;
    protected ILifecycle iLifecycle;
    protected Lifecycle.Event initializeRule = null;
    protected int defaultPosition = 0;
    protected CircularEffect circularEffect = null;
    protected Boolean isMaterial = false;
}
