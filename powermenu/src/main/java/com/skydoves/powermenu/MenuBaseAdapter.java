/*
 * Copyright (C) 2017 skydoves
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.skydoves.powermenu;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * MenuBaseAdapter is the base adapter class for {@link CustomPowerMenu}.
 */
public class MenuBaseAdapter<T> extends BaseItemProvider implements IMenuItem<T> {
    protected List<T> itemList;
    private ListContainer listView;

    private int selectedPosition = -1;
    private String preferenceName;
    private ListContainer.ItemClickedListener itemClickListener;

    public MenuBaseAdapter() {
        super();
        this.itemList = new ArrayList<>();
    }

    public MenuBaseAdapter(ListContainer listView) {
        super();
        this.itemList = new ArrayList<>();
        this.listView = listView;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int index) {
        return itemList.get(index);
    }

    @Override
    public long getItemId(int index) {
        return index;
    }

    @Override
    public Component getComponent(int index, Component component, ComponentContainer componentContainer) {
        if (component != null && listView != null) {
            listView.setItemClickedListener(itemClickListener);
        }
        return component;
    }

    @Override
    public void addItem(T item) {
        this.itemList.add(item);
        notifyDataChanged();
    }

    @Override
    public void addItem(int position, T item) {
        this.itemList.add(position, item);
        notifyDataChanged();
    }

    @Override
    public void addItemList(List<T> itemList) {
        this.itemList.addAll(itemList);
        notifyDataChanged();
    }

    @Override
    public int getSelectedPosition() {
        return this.selectedPosition;
    }

    @Override
    public void setSelectedPosition(int position) {
        this.selectedPosition = position;
        MenuPreferenceManager instance = MenuPreferenceManager.getInstance();
        if (instance != null && preferenceName != null) {
            instance.setPosition(preferenceName, position);
        }
    }

    @Override
    public ListContainer getListView() {
        return this.listView;
    }

    @Override
    public void setListView(ListContainer listView) {
        this.listView = listView;
    }

    @Override
    public void removeItem(T item) {
        this.itemList.remove(item);
    }

    @Override
    public void removeItem(int position) {
        this.itemList.remove(position);
    }

    @Override
    public void clearItems() {
        this.itemList.clear();
        notifyDataChanged();
    }

    @Override
    public List<T> getItemList() {
        return itemList;
    }

    @Override
    public void setItemClickListener(ListContainer.ItemClickedListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setPreference(String preferenceName) {
        this.preferenceName = preferenceName;
    }

    public String getPreferenceName() {
        return preferenceName;
    }
}
