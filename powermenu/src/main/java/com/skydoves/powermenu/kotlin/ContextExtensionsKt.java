package com.skydoves.powermenu.kotlin;

import ohos.aafwk.ability.Ability;
import ohos.app.Context;

public class ContextExtensionsKt {
    public static boolean isFinishing(Context context) {
        return context instanceof Ability && ((Ability) context).isTerminating();
    }
}
