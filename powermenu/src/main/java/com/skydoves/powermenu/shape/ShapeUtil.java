/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.skydoves.powermenu.shape;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.ComponentState;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.Arc;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * ShapeElement工具类
 */
public class ShapeUtil {
    /**
     * 获取颜色值的ShapeElement
     *
     * @param bgColor 0x***
     * @return ShapeElement
     */
    public static ShapeElement getShapeElement(int bgColor) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(bgColor));
        return shapeElement;
    }

    /**
     * 获取方形圆角ShapeElement
     *
     * @param color  颜色值
     * @param radius 四角圆形的半径
     * @return ShapeElement
     */
    public static ShapeElement createDrawable(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.RECTANGLE);
        if (color != 0) {
            drawable.setRgbColor(new RgbColor(color));
        }
        drawable.setCornerRadius(radius);
        return drawable;
    }

    /**
     * 获取方形圆角ShapeElement
     *
     * @param drawable ShapeElement
     * @param radius   四角圆形的半径
     * @return ShapeElement
     */
    public static ShapeElement createDrawable(ShapeElement drawable, float radius) {
        drawable.setCornerRadius(radius);
        return drawable;
    }

    /**
     * 获取方形圆角ShapeElement
     *
     * @param color    颜色值
     * @param ltRadius 四角圆形的左上半径
     * @param rtRadius 四角圆形的右上半径
     * @param rbRadius 四角圆形的右下半径
     * @param lbRadius 四角圆形的左下半径
     * @return ShapeElement
     */
    public static ShapeElement createRectangleCornersDrawable(int color, float ltRadius, float rtRadius, float rbRadius, float lbRadius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.RECTANGLE);
        if (color != 0) {
            drawable.setRgbColor(new RgbColor(color));
        }
        drawable.setCornerRadiiArray(
                new float[]{
                        ltRadius, ltRadius,
                        rtRadius, rtRadius,
                        rbRadius, rbRadius,
                        lbRadius, lbRadius
                });
        return drawable;
    }

    /**
     * 获取原件的ShapeElement
     *
     * @param drawable ShapeElement
     * @param tlRadius 左上圆角半径
     * @param trRadius 右上圆角半径
     * @param brRadius 右下圆角半径
     * @param blRadius 左下圆角半径
     * @return ShapeElement
     */
    public static ShapeElement createRectangleCornersDrawable(
            ShapeElement drawable, float tlRadius, float trRadius, float brRadius, float blRadius) {
        drawable.setCornerRadiiArray(
                new float[]{
                        tlRadius, tlRadius,
                        trRadius, trRadius,
                        brRadius, brRadius,
                        blRadius, blRadius
                });
        return drawable;
    }

    /**
     * 获取圆形的ShapeElement
     *
     * @param color    颜色值
     * @param tlRadius 圆形半径，最好是高度的一半
     * @return ShapeElement
     */
    public static ShapeElement createCircleDrawable(int color, float tlRadius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.OVAL);
        drawable.setRgbColor(new RgbColor(color));
        drawable.setCornerRadiiArray(
                new float[]{
                        tlRadius, tlRadius,
                        tlRadius, tlRadius,
                        tlRadius, tlRadius,
                        tlRadius, tlRadius
                });
        return drawable;
    }

    /**
     * 获取渐变色的ShapeElement
     *
     * @param rgbColors 颜色值数组
     * @return ShapeElement
     */
    public static ShapeElement createDrawable(RgbColor[] rgbColors) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.RECTANGLE);
        drawable.setOrientation(ShapeElement.Orientation.TOP_LEFT_TO_BOTTOM_RIGHT);
        drawable.setRgbColors(rgbColors);
        drawable.setArc(new Arc(0, 315, true));
        return drawable;
    }

    /**
     * 获取shape xml的ShapeElement
     *
     * @param xmlResId resId
     * @param context  上下文
     * @return ShapeElement
     */
    public static ShapeElement getXmlBgElement(Context context, int xmlResId) {
        return new ShapeElement(context, xmlResId);
    }

    /**
     * selector element
     *
     * @param pressColor  按下颜色
     * @param normalColor 默认颜色
     * @return StateElement
     */
    public static StateElement getSelectorElement(int pressColor, int normalColor) {
        StateElement stateElement = new StateElement();
        stateElement.addState(
                new int[]{ComponentState.COMPONENT_STATE_PRESSED},
                createDrawable(pressColor, 25)
        );
        stateElement.addState(
                new int[]{},
                createDrawable(normalColor, 25)
        );
        return stateElement;
    }

}