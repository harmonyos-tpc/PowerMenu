/*
 * Copyright (C) 2017 skydoves
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.skydoves.powermenu;

import com.skydoves.powermenu.annotations.Dp;
import com.skydoves.powermenu.annotations.Sp;
import com.skydoves.powermenu.shape.ShapeUtil;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * MenuListAdapter extends {@link MenuBaseAdapter}.
 * <p>
 * <p,
 * "value": "This is the {@link PowerMenu}'s default adapter.
 */
public class MenuListAdapter extends MenuBaseAdapter<PowerMenuItem> implements IPowerMenuAdapter {
    private int textColor = -2;
    private int menuColor = -2;
    private int selectedTextColor = -2;
    private int selectedMenuColor = -2;
    private int iconColor = -2;
    @Sp
    private int textSize = 12;
    @Dp
    private int iconSize = 35;
    @Dp
    private int iconPadding = 7;
    private int textGravity = TextAlignment.START;
    private Font textFont = null;

    private float radius;
    private boolean selectedEffect = true;

    public MenuListAdapter(ListContainer listView) {
        super(listView);
    }

    public MenuListAdapter(ListContainer listView, List<PowerMenuItem> list) {
        super(listView);
        itemList = list;
    }

    @Override
    public Component getComponent(int index, Component view, ComponentContainer viewGroup) {
        final Context context = viewGroup.getContext();
        if (view == null) {
            LayoutScatter inflater = LayoutScatter.getInstance(context);
            view = inflater.parse(ResourceTable.Layout_item_power_menu_library_skydoves, viewGroup, false);
        }
        PowerMenuItem powerMenuItem = (PowerMenuItem) getItem(index);
        final Component background = view.findComponentById(ResourceTable.Id_item_power_menu_layout);
        final Text title = (Text) view.findComponentById(ResourceTable.Id_item_power_menu_title);
        final Image icon = (Image) view.findComponentById(ResourceTable.Id_item_power_menu_icon);
        title.setText(powerMenuItem.title.toString());
        title.setTextSize(textSize, Text.TextSizeType.FP);
        title.setTextAlignment(textGravity);
        if (textFont != null) {
            title.setFont(textFont);
        }
        if (powerMenuItem.icon != 0) {
            ComponentContainer.LayoutConfig layoutConfig = icon.getLayoutConfig();
            layoutConfig.width = ConvertUtil.convertDpToPixel(iconSize, context);
            layoutConfig.height = ConvertUtil.convertDpToPixel(iconSize, context);
            icon.setImageAndDecodeBounds(powerMenuItem.icon);
            if (icon.getLayoutConfig() instanceof ComponentContainer.LayoutConfig) {
                layoutConfig.setMarginRight(ConvertUtil.convertDpToPixel(iconPadding, context));
            }
            icon.setLayoutConfig(layoutConfig);
            icon.setVisibility(Component.VISIBLE);
        } else {
            icon.setVisibility(Component.HIDE);
        }
        if (powerMenuItem.isSelected) {
            setSelectedPosition(index);
            if (selectedMenuColor == -2) {
                background.setBackground(getBackground(index, Color.WHITE.getValue()));
            } else {
                background.setBackground(getBackground(index, selectedMenuColor));
            }

            if (selectedTextColor == -2) {
                title.setTextColor(Color.BLACK);
            } else {
                title.setTextColor(new Color(selectedTextColor));
            }
        } else {
            if (menuColor == -2) {
                background.setBackground(getBackground(index, Color.WHITE.getValue()));
            } else {
                background.setBackground(getBackground(index, menuColor));
            }
            if (textColor == -2) {
                title.setTextColor(Color.BLACK);
            } else {
                title.setTextColor(new Color(textColor));
            }
        }
        return super.getComponent(index, view, viewGroup);
    }

    private ShapeElement getBackground(int index, int color) {
        ShapeElement element = new ShapeElement();
        if (index == 0) {
            element = ShapeUtil.createRectangleCornersDrawable(color, radius, radius, 0, 0);
        } else if (index == itemList.size() - 1) {
            element = ShapeUtil.createRectangleCornersDrawable(color, 0, 0, radius, radius);
        } else {
            element.setRgbColor(new RgbColor(color));
        }
        return element;
    }

    @Override
    public void setSelectedPosition(int position) {
        super.setSelectedPosition(position);
    }

    @Override
    public void setTextColor(int color) {
        this.textColor = color;
    }

    @Override
    public void setMenuColor(int color) {
        this.menuColor = color;
    }

    @Override
    public void setSelectedTextColor(int color) {
        this.selectedTextColor = color;
    }

    @Override
    public void setSelectedMenuColor(int color) {
        this.selectedMenuColor = color;
    }

    @Override
    public void setSelectedEffect(boolean selectedEffect) {
        this.selectedEffect = selectedEffect;
    }

    @Override
    public void setTextSize(@Sp int size) {
        this.textSize = size;
    }

    @Override
    public void setIconSize(int iconSize) {
        this.iconSize = iconSize;
    }

    @Override
    public void setIconColor(int iconColor) {
        this.iconColor = iconColor;
    }

    @Override
    public void setIconPadding(int iconPadding) {
        this.iconPadding = iconPadding;
    }

    @Override
    public void setRadius(float radius) {
        this.radius = radius;
    }

    @Override
    public void setTextGravity(int gravity) {
        this.textGravity = gravity;
    }

    @Override
    public void setTextFont(Font Font) {
        this.textFont = Font;
    }
}
