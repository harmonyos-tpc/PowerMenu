## PowerMenu
实现现代材质弹出菜单的最强大，最简单的方法。
PowerMenu可以完全自定义，并用于弹出对话框

<img src="gifs/powerMenu.gif" width="50%" height="50%"/>

## Gradle
 add a dependency code to your module's build.gradle file.
```
dependencies {
  implementation 'io.openharmony.tpc.thirdlib:PowerMenu:1.0.0'
}
```

## entry run

通过DevEco studio,并下载SDK

将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）

## Usage

### Basic example
This is a basic example on a screenshot. Here is how to create `PowerMenu` using `PowerMenu.Builder`.
```java
PowerMenu powerMenu = new PowerMenu.Builder(context)
          .addItemList(list) // list has "Novel", "Poerty", "Art"
          .addItem(new PowerMenuItem("Journals", false)) // add an item.
          .addItem(new PowerMenuItem("Travel", false)) // aad an item list.
          .setAnimation(MenuAnimation.SHOWUP_TOP_LEFT) // Animation start point (TOP | LEFT).
          .setMenuRadius(10f) // sets the corner radius.
          .setMenuShadow(10f) // sets the shadow.
          .setTextColor(ContextCompat.getColor(context, ResourceTable.Color_md_grey_800))
          .setTextGravity(Gravity.CENTER)
          .setSelectedTextColor(Color.WHITE.getValue())
          .setMenuColor(Color.WHITE.getValue())
          .setSelectedMenuColor(ContextCompat.getColor(context, ResourceTable.Color_colorPrimary))
          .setOnMenuItemClickListener(onMenuItemClickListener)
          .build();
```

We can add an item or an item list using `PowerMenuItem` class. This is how to initialize `PowerMenuItem`.
```java
new PowerMenuItem("Travel");
new PowerMenuItem("Poetery", false); // item name, isSelected (default is false).
new PowerMenuItem("Art", ResourceTable.Media_icon_art) // item name, item menu icon.
new PowerMenuItem("Travel", ResourceTable.Media_icon_travel, true) // item name, item menu icon, isSelected .
```

`OnMenuItemClickListener` is for listening to the item click of the popup menu.
```java
private OnMenuItemClickListener<PowerMenuItem> onMenuItemClickListener = new OnMenuItemClickListener<PowerMenuItem>() {
    @Override
    public void onItemClick(int position, PowerMenuItem item) {
        new ToastDialog(getContext()).setText(item.getTitle()).show();
        powerMenu.setSelectedPosition(position); // change selected item
        powerMenu.dismiss();
    }
};
```

After implementing the listener, we should set using `setOnMenuItemClickListener` method.
```java
.setOnMenuItemClickListener(onMenuItemClickListener)
```

The last, show the popup!
```java
powerMenu.showAsDropDown(view,xOff,yOff); // view is an anchor
```

And we should create TransPowerMenu.

```java
public class MenuListAdapter extends MenuBaseAdapter<PowerMenuItem> implements IPowerMenuAdapter {

    @Override
    public Component getComponent(int index, Component view, ComponentContainer viewGroup) {
        final Context context = viewGroup.getContext();
        if (view == null) {
            LayoutScatter inflater = LayoutScatter.getInstance(context);
            view = inflater.parse(ResourceTable.Layout_item_power_menu_library_skydoves, viewGroup, false);
        }
        PowerMenuItem powerMenuItem = (PowerMenuItem) getItem(index);
        final Component background = view.findComponentById(ResourceTable.Id_item_power_menu_layout);
        final Text title = (Text) view.findComponentById(ResourceTable.Id_item_power_menu_title);
        final Image icon = (Image) view.findComponentById(ResourceTable.Id_item_power_menu_icon);
        title.setText(powerMenuItem.title.toString());
        title.setTextSize(textSize, Text.TextSizeType.FP);
        title.setTextAlignment(textGravity);
        // --- skipped
        return super.getComponent(index, view, viewGroup);
    }
}
```

The last, create the `TransPowerMenu` with the `onMenuItemClickListener`.
```java
TransPowerMenu transPowerMenu = new TransPowerMenu.Builder(context)
        .setHeaderView(ResourceTable.Layout_item_title_header)
        .addItem(new PowerMenuItem("Profile", false))
        .addItem(new PowerMenuItem("Board", false))
        .addItem(new PowerMenuItem("Logout", false))
        .setLifecycle(lifecycleOwner)
        .setAnimation(MenuAnimation.SHOWUP_TOP_RIGHT)
        .setCircularEffect(CircularEffect.BODY)
        .setMenuRadius(10f)
        .setMenuShadow(10f)
        .setTextColor(context.getColor(ResourceTable.Color_md_grey_800))
        .setTextGravity(TextAlignment.CENTER)
        .setMenuColor(Color.BLACK.getValue())
        .setSelectedEffect(false)
        .setShowBackground(false)
        .setFocusable(true)
        .setOnMenuItemClickListener(onMenuItemClickListener)
        .build();
```
```java
private final OnMenuItemClickListener<PowerMenuItem> onProfileItemClickListener =
            new OnMenuItemClickListener<PowerMenuItem>() {
                @Override
                public void onItemClick(int position, PowerMenuItem item) {
                    new ToastDialog(getContext()).setText(item.getTitle().toString()).show();
                    profileMenu.dismiss();
                }
            };
```

## Preference
PowerMenu supports saving of the last selected menu and recovering as lifecycle.<br>
Here is how to save and recover selected menu.
```java
return new PowerMenu.Builder(context)
    // saves the position automatically when the menu is selected.
    // If we set the same preference name on the other PowerMenus, they will share the saving position.
   .setPreferenceName("HamburgerPowerMenu")

   .setInitializeRule(Lifecycle.Event.ON_CREATE, 0) // Lifecycle.Event and default position.
   --- skips ---
```

Here are the methods related to preference.
```java
.getPreferenceName() // gets the preference name of PowerMenu.
.getPreferencePosition(int defaultPosition) // gets the saved preference position from the SharedPreferences.
.setPreferencePosition(int defaultPosition) // sets the preference position name for persistence manually.
.clearPreference() // clears the preference name of PowerMenu.
```

And we can create a customized dialog like below.
```java
CustomPowerMenu customPowerMenu = new CustomPowerMenu.Builder<>(context, new CustomDialogMenuAdapter())
        .setHeaderView(ResourceTable.Layout_layout_custom_dialog_header)
        .setFooterView(ResourceTable.Layout_layout_custom_dialog_footer)
        .addItem(
             new NameCardMenuItem(
             getPixelMapDrawable(context, ResourceTable.Media_face3),
            "Sophie",
            context.getString(ResourceTable.String_board3))
            )
        .setILifecycle(lifecycleOwner)
        .setAnimation(MenuAnimation.SHOW_UP_CENTER)
        .setCircularEffect(CircularEffect.BODY)
        .setWidth(800)
        .setMenuRadius(10f)
        .setMenuShadow(10f)
        .setMenuAligment(LayoutAlignment.CENTER)
        .build();
```

## Background
These are options for the background.
```java
.setShowBackground(false) // do not showing background.
.setFocusable(true) // makes focusing only on the menu popup.
```

## Functions
### PowerMenu methods
```java
.addItemList(list) // add a PowerMenuItem list.
.addItem(new PowerMenuItem("Journals", false)) // add a PowerMenuItem.
.addItem(3, new PowerMenuItem("Travel", false)) // add a PowerMenuItem at position 3.
.setILifecycle(lifecycleOwner) // set LifecycleOwner for preventing memory leak.
.setWidth(300) // sets the popup width size.
.setHeight(400) // sets the popup height size.
.setMenuRadius(10f) // sets the popup corner radius.
.setMenuShadow(10f) // sets the popup shadow.
.setAnimation(MenuAnimation.FADE) // sets animations of the popup. It will start up when the popup is showing.
.setTextColor(ContextCompat.getColor(context, ResourceTable.Color_md_grey_800)) // sets the color of the default item text.
.setTextSize(12) // sets a text size of the item text
.setTextGravity(Gravity.CENTER) // sets a gravity of the item text.
.setSelectedTextColor(Color.WHITE.getValue()) // sets the color of the selected item text.
.setMenuColor(Color.WHITE.getValue()) // sets the color of the menu item color.
.setSelectedMenuColor(ContextCompat.getColor(context, ResourceTable.Color_colorPrimary)) // sets the color of the selected menu item color.
.setSelectedEffect(false) // sets the selected effects what changing colors of the selected menu item.
.setOnMenuItemClickListener(onMenuItemClickListener) // sets an item click listener.
.setOnDismissListener(OnDismissedListener onDismissListener) // sets a menu dismiss listener.
.setHeaderView(View view) //  sets the header view of the popup menu list.
.setHeaderView(int layout) // sets the header view of the popup menu using layout.
.setFooterView(View view) // sets the footer view of the popup menu list.
.setFooterView(int layout) // sets the footer view of the popup menu using layout.
.setSelection(int position) // sets the selected position of the popup menu. It can be used for scrolling as the position.
.getSelectedPosition() // gets the selected item position. if not selected before, returns -1 .
.getHeaderView() // gets the header view of the popup menu list.
.getFooterView() // gets the footer view of the popup menu list.
.getMenuListView() // gets the ListView of the popup menu.
```

### Background methods
```java
.setBackgroundColor(Color.GRAY.getValue()) // sets the color of the background.
.setShowBackground(false) // sets the background is showing or not.
.setOnBackgroundClickListener(onClickListener) // sets the background click listener of the background.
```

### Show & Dismiss
```java
.showAsDropDown(View anchor); // showing the popup menu as drop down to the anchor.
.showAsDropDown(View anchor, -370, 0); // showing the popup menu as drop down to the anchor with x-off and y-off.
.showAtCenter(View layout); // showing the popup menu as center aligns to the anchor.
.isShowing(); // gets the popup is showing or not.
.dismiss(); // dismiss the popup.
```


# License
```xml
Copyright 2017 skydoves

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
